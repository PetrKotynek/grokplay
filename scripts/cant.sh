#/bin/bash
# project
cd /home/vagrant
sudo wget  https://github.com/cantaloupe-project/cantaloupe/releases/download/v5.0.5/cantaloupe-5.0.5.zip
unzip -j -o cantaloupe-5.0.5.zip  -d ./
git clone https://github.com/GrokImageCompression/grok.git
mkdir grok/out
cd grok/out
cmake /home/vagrant/grok
#cmake -DBUILD_SHARED_LIBS=ON /home/vagrant/grok
make
sudo make install
cd /home/vagrant
java -Dcantaloupe.config=./cantaloupe.properties -Xmx2g -jar cantaloupe-5.0.5.jar
##yes y | curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh | /bin/bash
##(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/vagrant/.profile
##eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
##brew install grokj2k
# make sure permissions are fine
sudo chown -hR vagrant:vagrant *
