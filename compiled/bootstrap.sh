#!/bin/bash

# Set apt-get for non-interactive mode
export DEBIAN_FRONTEND=noninteractive

#echo "LANG=en_US.UTF-8" >> /etc/environment
#echo "LANGUAGE=en_US.UTF-8" >> /etc/environment
#echo "LC_ALL=en_US.UTF-8" >> /etc/environment
#echo "LC_CTYPE=en_US.UTF-8" >> /etc/environment
#sudo locale-gen en_US.UTF-8
sudo sh -c 'echo en_US.UTF-8 UTF-8 >>/etc/locale.gen' && sudo /usr/sbin/locale-gen

#sudo apt-mark hold zlib1g zlib1g-dev
# Update
apt-get -y update && apt-get -y upgrade

# SSH
apt-get -y install openssh-server

# Build tools
apt-get -y install build-essential automake libtool
apt-get -y install nmap mc
#apt-get -y default-jdk

# Git vim
apt-get -y install git vim

# Java 
apt-get install -y software-properties-common
#apt-get install -y python-software-properties
apt-get update

# Set JAVA_HOME variable both now and for when the system restarts
export JAVA_HOME
JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")
echo "JAVA_HOME=$JAVA_HOME" >> /etc/environment

# Maven
apt-get -y install maven libmaven-enforcer-plugin-java

# Wget and curl
apt-get -y install wget curl

# Bug fix for Ubuntu 14.04 with zsh 5.0.2 -- https://bugs.launchpad.net/ubuntu/+source/zsh/+bug/1242108
export MAN_FILES
MAN_FILES=$(wget -qO- "http://sourceforge.net/projects/zsh/files/zsh/5.0.2/zsh-5.0.2.tar.gz/download" \
  | tar xvz -C /usr/share/man/man1/ --wildcards "zsh-5.0.2/Doc/*.1" --strip-components=2)
for MAN_FILE in $MAN_FILES; do gzip /usr/share/man/man1/"${MAN_FILE##*/}"; done

# More helpful packages
apt-get -y install htop tree zsh
apt-get -y install libjpeg-dev zlib1g-dev 
apt-get -y install cmake pkg-config
#apt-get -y install binaryen
apt-get -y install libturbojpeg-java
#apt-get -y install libturbojpeg-java-dev
#apt-get -y install openjpeg2
apt-get  -y install libopenjp2-7-dev
apt-get  -y install ffmpeg
#apt-get  -y install libjpeg-turbo
apt-get  -y install libjpeg-dev
apt-get  -y install libjpeg-turbo-progs
apt-get  -y  install libjpeg62-turbo
apt-get  -y  install libturbojpeg0-dev
apt-get  -y  install libopenjp2-tools
apt-get -y install grokj2k-tools
apt-get  -y  install nasm
apt-get -y upgrade

